<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class TensesController extends Controller
{
    function index(){
	    $tenses = DB::table('tb_tenses')->get();
        return view('tenses/index', ['tenses' => $tenses]);
    }

    function detail($id){
        $get_tenses = DB::table('tb_tenses')->where('id', $id)->first();
        return view('tenses/detail', ['tenses' => $get_tenses]);
    }

    function add(){
    	return view('tenses/add');
    }

    function proc_add(Request $r){
        $nama_tenses  	= $r->input('nama');
        $deskripsi      = $r->input('deskripsi');
	    $data = array(
			'nama_tenses'	=>$nama_tenses,
			'deskripsi'		=>$deskripsi,
		);
		DB::table('tb_tenses')->insert($data);
		Session::flash('type','success');
		Session::flash('message','Tenses Added');
        return redirect()->to('tenses');
    }

    function edit($id){
        $get_tenses = DB::table('tb_tenses')->where('id', $id)->first();
        return view('tenses/edit', ['tenses' => $get_tenses]);
    }

    function proc_update(Request $r){
        $id             = $r->input('id');
        $nama_tenses    = $r->input('nama');
        $deskripsi      = $r->input('deskripsi');
        $data = array(
            'nama_tenses'   =>$nama_tenses,
            'deskripsi'     =>$deskripsi,
        );
        DB::table('tb_tenses')->where('id', $id)->update($data);
        Session::flash('type','success');
        Session::flash('message','Tenses Updated');
        return redirect()->to('tenses');
    }

    function delete($id){
        DB::table('tb_tenses')->where('id', $id)->delete();
        Session::flash('type','success');
        Session::flash('message','Tenses deleted');
        return redirect()->to('tenses');
    }

    function generate_pass(){
        $pwd            = "erika";
        $options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $password = password_hash($pwd, PASSWORD_BCRYPT, $options);
        // dd($password);
    }
}
