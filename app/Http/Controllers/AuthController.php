<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class AuthController extends Controller
{

	function do_login(Request $r){
		$username = $r->input('username');
		$password = $r->input('password');
		$get_data = DB::table('users')->where('username', $username)->first();
		$get_name   = DB::table('tb_siswa')->where('nis', $username)->first();
		$nama_siswa = '';
		if(!empty($get_name)){
			$nama_siswa = $get_name->nama;		
		}
		if(!empty($get_data)){
					$db_pass  = $get_data->password;
					if(password_verify($password, $db_pass)) {
						Session::put('logged_in',TRUE);
						Session::put('role_id',$get_data->role_id);
						Session::put('id',$get_data->id);
						Session::put('username',$get_data->username);
						Session::put('nama_siswa',$nama_siswa);
						Session::flash('type','success');
						Session::flash('message','Login Berhasil');
				        return redirect()->to('dashboard');
					}else{
						Session::flash('type','error');
						Session::flash('message','Login gagal !');						
				        return redirect()->to('err_login');
					}
				}else {
						Session::flash('type','error');
						Session::flash('message','Login gagal !');
				        return redirect()->to('err_login');
			}
	}

	function err_login(){
		return view('login');
	}

	function logout(){
		Session::flush();
		return redirect('/');
	}

	function change_password(){
		return view('change_password');
	}		

	function save_password(Request $r){
        $id            	= $r->input('id_user');
        $pwd            = $r->input('first_password');
		$second_password= $r->input('second_password');
        $options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $password = password_hash($pwd, PASSWORD_BCRYPT, $options);
        if($pwd != $second_password){
        	$type 		= "error";
        	$message 	= "Password tidak sama";
        }else{
        	$type = "success";
        	$message 	= "Password berhasil diganti";
		    $data = array(
		    			'password'			=>$password
		    		);
	        DB::table('users')->where('username',$id)->update($data);    
        }
		Session::flash('type',$type);
		Session::flash('message',$message);
        return redirect()->to('auth/change_password');
	}		

}
