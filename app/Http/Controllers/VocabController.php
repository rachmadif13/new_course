<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class VocabController extends Controller
{
    function index(){
	    $vocab = DB::table('tb_vocab')->get();
        return view('vocab/index', ['vocab' => $vocab]);
    }

    function add(){
    	return view('vocab/add');
    }

    function proc_add(Request $r){
        $bentuk_dasar  	= $r->input('bentuk_dasar');
        $past_simple    = $r->input('past_simple');
        $past_participle= $r->input('past_participle');
        $arti    		= $r->input('arti');
	    $data = array(
			'bentuk_dasar'		=>$bentuk_dasar,
			'past_simple'		=>$past_simple,
			'past_participle'	=>$past_participle,
			'arti'				=>$arti,
		);
		DB::table('tb_vocab')->insert($data);
		Session::flash('type','success');
		Session::flash('message','Vocab Added');
        return redirect()->to('vocab');
    }

    function delete($id){
        DB::table('tb_vocab')->where('id', $id)->delete();
        Session::flash('type','success');
        Session::flash('message','Vocab deleted');
        return redirect()->to('vocab');
    }

}
