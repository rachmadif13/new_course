<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class RekapitulasiController extends Controller
{
    function index(){
        $list = DB::table('tb_skor')
                ->select(DB::raw('max(level) as level'), 'id_siswa', 'skor','created_at')
                ->groupBy('id_siswa')
                ->orderBy(DB::raw('max(level)'), 'desc')
                ->get();

        return view('rekapitulasi/index', ['list_skor'=>$list]); 
    }

    function rekap_materi(){
           $list = DB::table('tb_skor_materi')
                ->select(DB::raw('max(level) as level'), 'id_siswa', 'skor','created_at')
                ->groupBy('id_siswa')
                ->orderBy(DB::raw('max(level)'), 'desc')
                ->get();

        return view('rekapitulasi/rekap_materi', ['list_skor'=>$list]); 	
    }

}