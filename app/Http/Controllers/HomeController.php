<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class HomeController extends Controller
{
	function index(){
		$count_siswa  = DB::table('tb_siswa')->count();
		$count_soal   = DB::table('tb_soal')->count();
		$count_materi = DB::table('tb_materi')->count();
		$count_tenses = DB::table('tb_tenses')->count();
		return view('dashboard')->with(['siswa'=>$count_siswa, 'soal'=>$count_soal, 'materi'=>$count_materi, 'tenses'=>$count_tenses ]);
	}
}
