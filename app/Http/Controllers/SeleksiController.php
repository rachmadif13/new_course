<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class SeleksiController extends Controller
{
	function index(){
        $seleksi  = DB::table('tb_level')->get();
        $cek_level = DB::table('tb_save_level')
                    ->select(DB::raw('max(level) as level'))
                    ->where('id_siswa',Session::get('username'))
                    ->first(); 
    	return view('seleksi/index', ['seleksi'=>$seleksi, 'level_now'=>$cek_level]);
	}

	function add($level){
    	return view('seleksi/add', ['level'=>$level]);
	}

	function proc_add(Request $r){
        $level      	 = $r->input('level');
        $jenis_soal      = $r->input('jenis_soal');
        $pertanyaan      = $r->input('pertanyaan');
        $pilihan_a       = $r->input('pilihan_a');
        $pilihan_b       = $r->input('pilihan_b');
        $pilihan_c       = $r->input('pilihan_c');
        $pilihan_d       = $r->input('pilihan_d');
        $pilihan_e       = $r->input('pilihan_e');
        $kunci           = $r->input('kunci');

        $t_pertanyaan   = array();
        $t_pilihan_a    = array();
        $t_pilihan_b    = array();
        $t_pilihan_c    = array();
        $t_pilihan_d    = array();
        foreach ($pertanyaan as $pertanyaan) {
            $t_pertanyaan[] = $pertanyaan;
        }
        
        foreach ($pilihan_a as $pilihan_a) {
            $t_pilihan_a[] = $pilihan_a;
        }

        foreach ($pilihan_b as $pilihan_b) {
            $t_pilihan_b[] = $pilihan_b;
        }

        foreach ($pilihan_c as $pilihan_c) {
            $t_pilihan_c[] = $pilihan_c;
        }

        foreach ($pilihan_d as $pilihan_d) {
            $t_pilihan_d[] = $pilihan_d;
        }

        foreach ($kunci as $kunci) {
            $t_kunci[] = $kunci;
        }

        $count_arr = count($t_pertanyaan);

        if($jenis_soal == 3){
            ini_set('memory_limit','256M');
            $audio                 = $r->file('audio');
            $input['nama_audio']   = $audio->getClientOriginalName();
            $lokasi                = public_path('upload_audio');
            $audio->move($lokasi, $input['nama_audio']);

            $data = array('audio'=>$input['nama_audio']);
            DB::table('tb_listening_group')->insert($data);        
            $id_audio = DB::select('SELECT id FROM tb_listening_group ORDER BY id DESC LIMIT 1');
            $id_audio = $id_audio[0]->id;
            $id_bacaan  = null;
        }elseif($jenis_soal == 2){
            $data = array('bacaan'=>$r->input('bacaan'));
            DB::table('tb_reading_group')->insert($data);        
            $id_bacaan = DB::select('SELECT id FROM tb_reading_group ORDER BY id DESC LIMIT 1');
            $id_bacaan = $id_bacaan[0]->id;
            $id_audio  = null;
        }else{
            $id_audio   = null;
            $id_bacaan  = null;
        }

           for($i=0 ; $i<$count_arr; $i++){
                $data = array(
                        'id_audio'      =>$id_audio,
                        'id_bacaan'     =>$id_bacaan,
                        'type'          =>$jenis_soal,
                        'level'         =>$level,
                        'pertanyaan'    =>$t_pertanyaan[$i],
                        'pilihan_a'     =>$t_pilihan_a[$i],
                        'pilihan_b'     =>$t_pilihan_b[$i],
                        'pilihan_c'     =>$t_pilihan_c[$i],
                        'pilihan_d'     =>$t_pilihan_d[$i],
                        'kunci_jawaban' =>$t_kunci[$i]
                    );
                DB::table('tb_soal')->insert($data);
            }

        Session::flash('type','success');
        Session::flash('message','Success Added');
        return redirect()->to('seleksi/kategori_soal/'.$level);
	}

	function kategori_soal($level){
        $data = DB::table('tb_type')
			        ->select(DB::raw('count(tb_soal.type) as jml_soal'), 'tb_soal.level','tb_type.deskripsi', 'tb_type.type')
                    ->leftjoin('tb_soal', 'tb_soal.type', '=', 'tb_type.type')
                    ->where('tb_soal.level', $level)
                    ->groupBy('tb_type.type')
                    ->orderBy('tb_soal.level', 'asc')
                    ->get();
        return view('seleksi/kategori_seleksi', ['data'=>$data, 'level'=>$level]);  
    }

    function detail($level, $type){
                $data = array();
                if($type == 2){
                    $query = DB::table('tb_soal')
                        ->select('tb_soal.*', 'tb_reading_group.bacaan')
                        ->leftjoin('tb_reading_group', 'tb_soal.id_bacaan', '=', 'tb_reading_group.id')
                        ->where('tb_soal.level', $level)
                        ->where('tb_soal.type', $type)
                        ->get();
                    foreach ($query as $key => $value) {
                        $data[$value->id_bacaan][] = $value;
                    }
                }elseif($type==3){
                    $query = DB::table('tb_soal')
                        ->select('tb_soal.*', 'tb_listening_group.audio')
                        ->leftjoin('tb_listening_group', 'tb_soal.id_audio', '=', 'tb_listening_group.id')
                        ->where('tb_soal.level', $level)
                        ->where('tb_soal.type', $type)
                        ->get();
                    foreach ($query as $key => $value) {
                        $data[$value->id_audio][] = $value;
                    }
                }else{
                    $query = DB::table('tb_soal')
                        ->select('tb_soal.*')
                        ->where('tb_soal.level', $level)
                        ->where('tb_soal.type', $type)
                        ->get();
                     foreach ($query as $key => $value) {
                        $data[$value->id_bacaan][] = $value;
                    }
                }
    	return view('seleksi/detail', ['data'=>$data, 'level'=>$level, 'type'=>$type]);	

	}

	function save_seleksi(Request $r){
		$pilihan 		= $r->input('pilihan');
		$type 		 	= $r->input('type');
		$level  		= $r->input('level');
		$id_soal 		= $r->input('id');
        for ($i=0;$i<count($id_soal);$i++){
                $nomor      = $id_soal[$i];
                $jawaban    = $pilihan[$nomor];
                $data       = array(
                               'kunci_jawaban' => $jawaban
                                );
                $query = DB::table('tb_soal')->where('id', $nomor)->update($data);
		}
        Session::flash('type','success');
        Session::flash('message','Success Updated');
        return redirect()->to('seleksi/detail/'.$level.'/'.$type);

	}

	function do_seleksi($level){
        $query = DB::table('tb_soal')
                        ->select('tb_soal.*', 'tb_reading_group.bacaan', 'tb_listening_group.audio')
                        ->leftjoin('tb_reading_group', 'tb_soal.id_bacaan', '=', 'tb_reading_group.id')
                        ->leftjoin('tb_listening_group', 'tb_soal.id_audio', '=', 'tb_listening_group.id')
                        ->where('tb_soal.level', $level)
                        ->inRandomOrder()->get();
        foreach ($query as $key => $value) {
                $data[$value->id_bacaan][] = $value;
        }
        return view('seleksi/do_seleksi', ['data'=>$data, 'level'=>$level]);    

    }

    function hitung_skor(Request $r){
        $pilihan        = $r->input('pilihan');
        $id_soal        = $r->input('id');
        $level          = $r->input('level');
        $score          = 0;
        $benar          = 0;
        $salah          = 0;
        $kosong         = 0;
        for ($i=0;$i<count($id_soal);$i++){
            $nomor = $id_soal[$i];
            if (empty($pilihan[$nomor])){
                $kosong++;
            }else{
                $jawaban=$pilihan[$nomor];
                $cek = DB::table('tb_soal')->where('id', $nomor)->where('kunci_jawaban', $jawaban)->count();
                if($cek){
                    $benar++;
                }else{
                    $salah++;
                }  
            } 
            $nilai = 100/count($id_soal)*$benar;
        }

            $data_nilai = array(
                'id_siswa' =>Session::get('username'),
                'skor'     =>$nilai,
                'level'    =>$level
            );
        DB::table('tb_skor')->insert($data_nilai);
        if($nilai >= 65){
            $cek_data = DB::table('tb_save_level')->where('id_siswa', Session::get('username'))->first();
             $data_up = array(
                        'level'    =>$level+1
                );
            if(empty($cek_data)){
                DB::table('tb_save_level')->insert($data_up);            
            }else{
                DB::table('tb_save_level')->where('id_siswa', Session::get('username'))->update($data_up);
            }
            
        }

        Session::flash('type','success');
        Session::flash('message','Seleksi Submited');
        return view('seleksi/hasil', ['nilai'=>$nilai, 'benar'=>$benar, 'salah'=>$salah, 'kosong'=>$kosong]);   
    }

    function list_skor($nis){
        $list = DB::table('tb_skor')->where('id_siswa', $nis)->orderBy('created_at', 'desc')->get();
    	return view('seleksi/list_skor', ['list_skor'=>$list]);	
        
    }

    function delete_soal($id, $lv, $type){
        DB::table('tb_soal')->where('id', $id)->delete();
        Session::flash('type','success');
        Session::flash('message','Soal deleted');
        return redirect()->to('seleksi/detail/'.$lv.'/'.$type);
    }

}
