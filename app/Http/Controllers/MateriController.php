<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class MateriController extends Controller
{

    function index(){
        $materi     = DB::table('tb_type')->get();
        $cek_materi = DB::table('tb_save_level_materi')
                    ->select(DB::raw('max(level) as level'))
                    ->where('id_siswa',Session::get('username'))
                    ->first(); 
        return view('materi/list', ['materi'=>$materi, 'level_now'=>$cek_materi]);
    }

    function writing(){
        $materi = DB::table('tb_materi')->where('type', 1)->get();
        $type = 'writing';
    	return view('materi/index', ['materi'=>$materi, 'type'=>$type]);
    }

    function reading(){
        $materi = DB::table('tb_materi')->where('type', 2)->get();
        $type = 'reading';
        return view('materi/index', ['materi'=>$materi, 'type'=>$type]);
    }

    function listening(){
        $materi = DB::table('tb_materi')->where('type', 3)->get();
        $type = 'listening';
        return view('materi/index', ['materi'=>$materi, 'type'=>$type]);
    }

    function add_materi($type){
    	return view('materi/add', ['type' => $type]);
    }

	function save_materi(Request $r){
        $type           = $r->input('type');
        $judul          = $r->input('judul');
        $deskripsi      = $r->input('deskripsi');
        $redir          = $type;
        $message        = $type;
        if($type == 'writing'){
            $type_in_db = 1;
        }elseif($type=='reading'){
            $type_in_db = 2;
        }else{
            $type_in_db = 3;
        }

        $data = array(
            'type'          =>$type_in_db,
            'judul_materi'  =>$judul,
            'deskripsi'     =>$deskripsi
        );
        DB::table('tb_materi')->insert($data);
        Session::flash('type','success');
        Session::flash('message',$message.' Added');
        return redirect()->to($redir);
	}

    function detail($id){
        $materi = DB::table('tb_materi')->where('id', $id)->first();
        $koment = DB::table('tb_comment')
                    ->leftjoin('users', 'tb_comment.id_user', '=', 'users.id')
                    ->leftjoin('tb_materi', 'tb_comment.id_materi', '=', 'tb_materi.id')
                    ->where('tb_materi.id', $id)
                    ->get();
        if(Session::get('role_id')==2){
          $cek          = DB::table('tb_materi')->where('id', $id)->first();
          $tot_lihat    = $cek->dilihat;
          $update_see   = array(
                'dilihat'     =>$tot_lihat+1
            );
            DB::table('tb_materi')->where('id', $id)->update($update_see);
        }
        return view('materi/detail', ['materi' => $materi, 'komentar'=>$koment]);
    }

    function edit($id){
        $materi = DB::table('tb_materi')->where('id', $id)->first();
        return view('materi/edit', ['materi' => $materi]);
    }

    function proc_update(Request $r){
            $type           = $r->input('type');
            $id             = $r->input('id');
            $judul          = $r->input('judul');
            $deskripsi      = $r->input('deskripsi');
            $data = array(
                'judul_materi'  =>$judul,
                'deskripsi'     =>$deskripsi
            );
            if($type == 1){
                $message    ='writing';
                $redir      ='writing';
            }elseif($type==2){
                $message    ='reading';
                $redir      ='reading';
            }else{
                $message    ='listening';
                $redir      ='listening';
            }
            DB::table('tb_materi')->where('id', $id)->update($data);
            Session::flash('type','success');
            Session::flash('message',$message.' Updated');
            return redirect()->to($redir);
        }

    function delete($id){
        $type = DB::table('tb_materi')->where('id', $id)->first();
        if($type->type == 1){
            $redir = 'writing';
        }elseif($type->type == 2){
            $redir = 'reading';
        }else{
            $redir = 'listening';
        }
        DB::table('tb_materi')->where('id', $id)->delete();
        Session::flash('type','success');
        Session::flash('message','Data success deleted');
        return redirect()->to($redir);
    }

    function komentar(Request $r){
        $id_user           = $r->input('id_user');
        $id_materi         = $r->input('id_materi');
        $pesan             = $r->input('pesan');
        $data = array(
            'id_user'        =>$id_user,
            'id_materi'      =>$id_materi,
            'pesan'          =>$pesan
        );
        DB::table('tb_comment')->insert($data);
        Session::flash('type','success');
        Session::flash('message','Koment Added');
        return redirect()->to('materi/detail/'.$id_materi);
    }

    function test($type){
        if($type == 'writing'){
            $x = 1;
        }elseif($type=='reading'){
            $x = 2;
        }elseif($type=='listening'){
            $x=3;
        }
       $query = DB::table('tb_soal')
                        ->select('tb_soal.*', 'tb_reading_group.bacaan')
                        ->leftjoin('tb_reading_group', 'tb_soal.id_bacaan', '=', 'tb_reading_group.id')
                        ->where('tb_soal.type', $x)
                        ->get();
        foreach ($query as $key => $value) {
                $data[$value->id_bacaan][] = $value;
        }
        return view('materi/test', ['data'=>$data, 'type'=>$type]);    
    }

    function hitung_skor(Request $r){
        $pilihan        = $r->input('pilihan');
        $id_soal        = $r->input('id');
        $level          = $r->input('level');
        $score          = 0;
        $benar          = 0;
        $salah          = 0;
        $kosong         = 0;
        for ($i=0;$i<count($id_soal);$i++){
            $nomor = $id_soal[$i];
            if (empty($pilihan[$nomor])){
                $kosong++;
            }else{
                $jawaban=$pilihan[$nomor];
                $cek = DB::table('tb_soal')->where('id', $nomor)->where('kunci_jawaban', $jawaban)->count();
                if($cek){
                    $benar++;
                }else{
                    $salah++;
                }  
            } 
            $nilai = 100/count($id_soal)*$benar;
        }

            $data_nilai = array(
                'id_siswa' =>Session::get('username'),
                'skor'     =>$nilai,
                'level'    =>$level
            );
        DB::table('tb_skor_materi')->insert($data_nilai);
        if($nilai >= 65){
            $cek_data = DB::table('tb_save_level_materi')->where('id_siswa', Session::get('username'))->first();
             $data_up = array(
                        'level'    =>$level+1
                );
            if(empty($cek_data)){
                DB::table('tb_save_level_materi')->insert($data_up);            
            }else{
                DB::table('tb_save_level_materi')->where('id_siswa', Session::get('username'))->update($data_up);
            }
            
        }
        Session::flash('type','success');
        Session::flash('dif','materi');
        Session::flash('message','Latihan Submited');
        return view('seleksi/hasil', ['nilai'=>$nilai, 'benar'=>$benar, 'salah'=>$salah, 'kosong'=>$kosong]);   
    }

    function detail_soal($type){
        if($type == 'writing'){
            $jenis = 1;
        }elseif($type == 'reading'){
            $jenis = 2;
        }elseif($type == 'listening'){
            $jenis = 3;            
        }
         $data = array();
            if($type == 'reading'){
                $query = DB::table('tb_soal_materi')
                    ->select('tb_soal_materi.*', 'tb_reading_group.bacaan')
                    ->leftjoin('tb_reading_group', 'tb_soal_materi.id_bacaan', '=', 'tb_reading_group.id')
                    ->where('tb_soal_materi.type', $jenis)
                    ->get();
                foreach ($query as $key => $value) {
                    $data[$value->id_bacaan][] = $value;
                }
            }elseif($type=='listening'){
                $query = DB::table('tb_soal_materi')
                    ->select('tb_soal_materi.*', 'tb_listening_group.audio')
                    ->leftjoin('tb_listening_group', 'tb_soal_materi.id_audio', '=', 'tb_listening_group.id')
                    ->where('tb_soal_materi.type', $jenis)
                    ->get();
                foreach ($query as $key => $value) {
                    $data[$value->id_audio][] = $value;
                }
            }else{
                $query = DB::table('tb_soal_materi')
                    ->select('tb_soal_materi.*')
                    ->where('tb_soal_materi.type', $jenis)
                    ->get();
                 foreach ($query as $key => $value) {
                    $data[$value->id_bacaan][] = $value;
                }
            }
        return view('materi/detail_soal', ['data'=>$data, 'type'=>$jenis, 'jenis'=>$type]); 
    }

    function add_soal(){
        return view('materi/add_soal');
    }

    function proc_add_soal(Request $r){
        $jenis_soal      = $r->input('jenis_soal');
        $pertanyaan      = $r->input('pertanyaan');
        $pilihan_a       = $r->input('pilihan_a');
        $pilihan_b       = $r->input('pilihan_b');
        $pilihan_c       = $r->input('pilihan_c');
        $pilihan_d       = $r->input('pilihan_d');
        $pilihan_e       = $r->input('pilihan_e');
        $kunci           = $r->input('kunci');

        $t_pertanyaan   = array();
        $t_pilihan_a    = array();
        $t_pilihan_b    = array();
        $t_pilihan_c    = array();
        $t_pilihan_d    = array();
        foreach ($pertanyaan as $pertanyaan) {
            $t_pertanyaan[] = $pertanyaan;
        }
        
        foreach ($pilihan_a as $pilihan_a) {
            $t_pilihan_a[] = $pilihan_a;
        }

        foreach ($pilihan_b as $pilihan_b) {
            $t_pilihan_b[] = $pilihan_b;
        }

        foreach ($pilihan_c as $pilihan_c) {
            $t_pilihan_c[] = $pilihan_c;
        }

        foreach ($pilihan_d as $pilihan_d) {
            $t_pilihan_d[] = $pilihan_d;
        }

        foreach ($kunci as $kunci) {
            $t_kunci[] = $kunci;
        }

        $count_arr = count($t_pertanyaan);

        if($jenis_soal == 3){
            $audio                 = $r->file('audio');
            $input['nama_audio']   = $audio->getClientOriginalName();
            $lokasi                = public_path('upload_audio');
            $audio->move($lokasi, $input['nama_audio']);

            $data = array('audio'=>$input['nama_audio']);
            DB::table('tb_listening_group')->insert($data);        
            $id_audio = DB::select('SELECT id FROM tb_listening_group ORDER BY id DESC LIMIT 1');
            $id_audio = $id_audio[0]->id;
            $id_bacaan  = null;
        }elseif($jenis_soal == 2){
            $data = array('bacaan'=>$r->input('bacaan'));
            DB::table('tb_reading_group')->insert($data);        
            $id_bacaan = DB::select('SELECT id FROM tb_reading_group ORDER BY id DESC LIMIT 1');
            $id_bacaan = $id_bacaan[0]->id;
            $id_audio  = null;
        }else{
            $id_audio   = null;
            $id_bacaan  = null;
        }

           for($i=0 ; $i<$count_arr; $i++){
                $data = array(
                        'id_audio'      =>$id_audio,
                        'id_bacaan'     =>$id_bacaan,
                        'type'          =>$jenis_soal,
                        'pertanyaan'    =>$t_pertanyaan[$i],
                        'pilihan_a'     =>$t_pilihan_a[$i],
                        'pilihan_b'     =>$t_pilihan_b[$i],
                        'pilihan_c'     =>$t_pilihan_c[$i],
                        'pilihan_d'     =>$t_pilihan_d[$i],
                        'kunci_jawaban' =>$t_kunci[$i]
                    );
                DB::table('tb_soal_materi')->insert($data);
            }

        Session::flash('type','success');
        Session::flash('message','Success Added');
        return redirect()->to('materi');
    }

    function proc_update_kunci(Request $r){
        $pilihan        = $r->input('pilihan');
        $type           = $r->input('type');
        $id_soal        = $r->input('id');
        for ($i=0;$i<count($id_soal);$i++){
                $nomor      = $id_soal[$i];
                $jawaban    = $pilihan[$nomor];
                $data       = array(
                               'kunci_jawaban' => $jawaban
                                );
                $query = DB::table('tb_soal_materi')->where('id', $nomor)->update($data);
        }
        Session::flash('type','success');
        Session::flash('message','Success Updated');
        return redirect()->to('materi/detail_soal/'.$type);

    }

    function delete_soal($id, $type){
        DB::table('tb_soal_materi')->where('id', $id)->delete();
        Session::flash('type','success');
        Session::flash('message','Soal deleted');
        if($type == 1 ){
            $jenis = 'writing';
        }elseif($type == 2){
            $jenis = 'reading';
        }elseif($type == 3){
            $jenis = 'listening';
        }
        return redirect()->to('materi/detail_soal/'.$jenis);
    }

}
