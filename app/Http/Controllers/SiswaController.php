<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class SiswaController extends Controller
{
    function index(){
	    $siswa = DB::table('tb_siswa')->get();
        return view('siswa/index', ['siswa' => $siswa]);
    }

    function add(){
    	return view('siswa/add');
    }

    function proc_add(Request $r){

        $nis            = $r->input('nis');
        $nama           = $r->input('nama');
        $jenis_kelamin  = $r->input('jenis_kelamin');
        $tgl_lahir      = $r->input('tgl_lahir');
        $alamat         = $r->input('alamat');
        $status         = $r->input('status');
        $pwd            = $r->input('password');
        $options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $password = password_hash($pwd, PASSWORD_BCRYPT, $options);

        $foto               = $r->file('foto');
        $input['nama_foto'] = time().'.'.$foto->getClientOriginalExtension();
        $lokasi             = public_path('upload_img');
        $foto->move($lokasi, $input['nama_foto']);

	    $data = array(
	    			'nis'			=>$nis,
	    			'nama'			=>$nama,
	    			'jenis_kelamin'	=>$jenis_kelamin,
	    			'tgl_lahir'		=>$tgl_lahir,
	    			'alamat'		=>$alamat,
                    'status'        =>$status,
	    			'foto'		    =>$input['nama_foto'],
	    		);
        $data_login = array(
            'username' => $nis,
            'password' => $password,
            'role_id' => 2
        );

         $data_def_level = array(
                    'id_siswa'    =>$nis,
                    'level'       =>1
                );
        DB::table('tb_save_level')->insert($data_def_level);    
        DB::table('tb_save_level_materi')->insert($data_def_level);    
        DB::table('users')->insert($data_login);
		DB::table('tb_siswa')->insert($data);
		Session::flash('type','success');
		Session::flash('message','Siswa Added');
        return redirect()->to('siswa');
    }

    function edit($nis){
        $get_siswa = DB::table('tb_siswa')->where('nis', $nis)->first();
        return view('siswa/edit', ['siswa' => $get_siswa]);
    }

    function proc_update(Request $r){
        $nis            = $r->input('nis');
        $nama           = $r->input('nama');
        $jenis_kelamin  = $r->input('jenis_kelamin');
        $tgl_lahir      = $r->input('tgl_lahir');
        $alamat         = $r->input('alamat');
        $status         = $r->input('status');
        $data = array(
                    'nama'          =>$nama,
                    'jenis_kelamin' =>$jenis_kelamin,
                    'tgl_lahir'     =>$tgl_lahir,
                    'alamat'        =>$alamat,
                    'status'        =>$status
                );
        DB::table('tb_siswa')->where('nis', $nis)->update($data);
        Session::flash('type','success');
        Session::flash('message','Siswa Updated');
        return redirect()->to('siswa');
    }

    function delete($nis){
        DB::table('tb_siswa')->where('nis', $nis)->delete();
        Session::flash('type','success');
        Session::flash('message','Siswa deleted');
        return redirect()->to('siswa');
    }
}
