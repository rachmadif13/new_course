-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.31-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk course
CREATE DATABASE IF NOT EXISTS `course` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `course`;

-- membuang struktur untuk table course.tb_comment
CREATE TABLE IF NOT EXISTS `tb_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pesan` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_materi` (`id_materi`),
  KEY `FK_tb_comment_users` (`id_user`),
  CONSTRAINT `FK_tb_comment_tb_materi` FOREIGN KEY (`id_materi`) REFERENCES `tb_materi` (`id`),
  CONSTRAINT `FK_tb_comment_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel course.tb_comment: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_comment` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_level
CREATE TABLE IF NOT EXISTS `tb_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'admin',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_level: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_level` DISABLE KEYS */;
INSERT INTO `tb_level` (`id`, `level`, `created_by`, `created_at`) VALUES
	(1, 1, 'admin', '2018-09-25 17:24:27'),
	(2, 2, 'admin', '2018-09-25 17:25:22'),
	(3, 3, 'admin', '2018-09-25 17:26:11');
/*!40000 ALTER TABLE `tb_level` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_listening_group
CREATE TABLE IF NOT EXISTS `tb_listening_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audio` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_listening_group: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_listening_group` DISABLE KEYS */;
INSERT INTO `tb_listening_group` (`id`, `audio`) VALUES
	(8, 'sao.mp3'),
	(9, 'sao2.mp3');
/*!40000 ALTER TABLE `tb_listening_group` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_materi
CREATE TABLE IF NOT EXISTS `tb_materi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_materi` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  `type` char(1) DEFAULT NULL COMMENT '1:writing; 2:reading; 3:listening',
  `created_by` varchar(50) DEFAULT 'admin',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dilihat` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_materi: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_materi` DISABLE KEYS */;
INSERT INTO `tb_materi` (`id`, `judul_materi`, `deskripsi`, `type`, `created_by`, `created_at`, `dilihat`) VALUES
	(8, 'Passive voice', '<p style="margin-bottom: 25px; padding: 0px;" open="" sans",="" sans-serif;"=""><font color="#555555">Kalimat pasif atau passive voice digunakan untuk menunjukkan ketertarikan pada seseorang atau objek yang dikenai tindakan dan bukan seseorang atau objek yang melakukan tindakan. Jadi, hal atau orang yang terpenting akan menjadi subjek kalimat.</font></p><p style="margin-bottom: 25px; padding: 0px;" open="" sans",="" sans-serif;"=""><font color="#555555">CONTOH:s</font></p><p style="margin-bottom: 25px; padding: 0px;" open="" sans",="" sans-serif;"=""><span style="color: rgb(85, 85, 85);">- The passive voice is used frequently. (= kita tertarik dengan kalimat pasif, bukan siapa yang menggunakannya.)</span></p><p style="margin-bottom: 25px; padding: 0px;" open="" sans",="" sans-serif;"=""><font color="#555555">- The house was built in 1654. (= kita tertarik dengan rumahnya, bukan siapa yang membangunnya.)</font></p><p style="margin-bottom: 25px; padding: 0px;" open="" sans",="" sans-serif;"=""><font color="#555555">- The road is being repaired. (= kita tertarik dengan jalannya, bukan siapa yang melakukan perbaikan.)</font></p>', '1', 'admin', '2018-10-21 13:04:28', 4),
	(12, 'listening sub materi updatef', '&nbsp;updated listening sub materi', '3', 'admin', '2018-10-21 13:04:53', 3),
	(13, 'Active voice', 'Ini active voice', '1', 'admin', '2018-10-21 13:05:11', 2),
	(14, 'reading first', '<p>test materi reading,</p><p>ini digunakan untuk tes enter</p>', '2', 'admin', '2018-10-21 13:05:02', 2);
/*!40000 ALTER TABLE `tb_materi` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_save_level
CREATE TABLE IF NOT EXISTS `tb_save_level` (
  `id_siswa` varchar(50) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_save_level: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_save_level` DISABLE KEYS */;
INSERT INTO `tb_save_level` (`id_siswa`, `level`) VALUES
	('123', 2),
	('135150218113024', 1),
	('135150218113025', 2);
/*!40000 ALTER TABLE `tb_save_level` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_siswa
CREATE TABLE IF NOT EXISTS `tb_siswa` (
  `nis` varchar(50) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0: tidak aktif ,1: aktif',
  `foto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_siswa: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_siswa` DISABLE KEYS */;
INSERT INTO `tb_siswa` (`nis`, `nama`, `jenis_kelamin`, `tgl_lahir`, `alamat`, `status`, `foto`) VALUES
	('123', 'siswa', 'L', '2017-10-31', 'sdaf', 0, '1540100226.jpg'),
	('135150218113024', 'rachmad idr', 'L', '1997-12-03', 'jombang', 1, '1538500348.jpg'),
	('135150218113025', 'erika kusuma', 'P', '2014-10-27', 'kediri', 1, '1539590296.jpg');
/*!40000 ALTER TABLE `tb_siswa` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_skor
CREATE TABLE IF NOT EXISTS `tb_skor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` varchar(50) DEFAULT NULL,
  `skor` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_skor: ~42 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_skor` DISABLE KEYS */;
INSERT INTO `tb_skor` (`id`, `id_siswa`, `skor`, `level`, `created_at`) VALUES
	(22, '135150218113024', 100, 1, '2018-10-03 18:35:03'),
	(23, '135150218113024', 20, 1, '2018-10-03 18:45:03'),
	(24, '135150218113024', 20, 1, '2018-10-03 18:55:03'),
	(25, '135150218113024', 50, 1, '2018-10-03 18:49:23'),
	(26, '135150218113024', 100, 1, '2018-10-03 18:49:42'),
	(27, '135150218113025', 100, 1, '2018-10-15 15:03:05'),
	(28, '135150218113025', 100, 1, '2018-10-15 15:12:56'),
	(29, '135150218113025', 100, 1, '2018-10-15 15:13:14'),
	(30, '135150218113025', 100, 1, '2018-10-15 15:22:09'),
	(31, '135150218113025', 100, 1, '2018-10-15 15:22:31'),
	(32, '135150218113025', 100, 1, '2018-10-15 15:23:11'),
	(33, '135150218113025', 100, 1, '2018-10-15 15:24:02'),
	(34, '135150218113025', 100, 1, '2018-10-15 15:24:16'),
	(35, '135150218113025', 100, 1, '2018-10-15 15:25:19'),
	(36, '135150218113025', 100, 1, '2018-10-15 15:25:30'),
	(37, '135150218113025', 100, 1, '2018-10-15 15:25:43'),
	(38, '135150218113025', 100, 1, '2018-10-15 15:25:50'),
	(39, '135150218113025', 100, 1, '2018-10-15 15:26:06'),
	(40, '135150218113025', 100, 1, '2018-10-15 15:26:18'),
	(41, '135150218113025', 100, 1, '2018-10-15 15:26:37'),
	(42, '135150218113025', 100, 1, '2018-10-15 15:33:00'),
	(43, '135150218113025', 100, 1, '2018-10-15 15:33:47'),
	(44, '135150218113025', 100, 1, '2018-10-15 15:34:29'),
	(45, '135150218113025', 100, 1, '2018-10-15 15:35:24'),
	(46, '135150218113025', 100, 1, '2018-10-15 15:35:44'),
	(47, '135150218113025', 100, 1, '2018-10-15 15:36:36'),
	(48, '135150218113025', 100, 1, '2018-10-15 15:37:32'),
	(49, '135150218113025', 100, 1, '2018-10-15 15:40:45'),
	(50, '135150218113025', 100, 1, '2018-10-15 15:45:13'),
	(51, '135150218113025', 100, 1, '2018-10-15 15:45:54'),
	(52, '135150218113025', 100, 1, '2018-10-15 15:47:04'),
	(53, '135150218113025', 100, 1, '2018-10-15 15:47:48'),
	(54, '135150218113025', 100, 1, '2018-10-15 15:48:28'),
	(55, '135150218113025', 100, 1, '2018-10-15 15:49:23'),
	(56, '135150218113025', 100, 1, '2018-10-15 15:51:42'),
	(57, '135150218113025', 100, 1, '2018-10-15 15:52:25'),
	(58, '135150218113025', 100, 1, '2018-10-15 15:55:08'),
	(59, '135150218113025', 100, 1, '2018-10-15 15:55:20'),
	(60, '135150218113025', 100, 1, '2018-10-15 15:55:50'),
	(61, '135150218113025', 100, 1, '2018-10-15 15:56:41'),
	(62, '135150218113025', 100, 1, '2018-10-15 15:58:03'),
	(63, '135150218113025', 100, 1, '2018-10-15 15:58:10'),
	(64, '123', 100, 1, '2018-10-21 12:39:24'),
	(65, '123', 100, 1, '2018-10-21 12:39:46'),
	(66, '123', 100, 1, '2018-10-21 13:25:00');
/*!40000 ALTER TABLE `tb_skor` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_soal
CREATE TABLE IF NOT EXISTS `tb_soal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` varchar(500) DEFAULT NULL,
  `pilihan_a` varchar(200) DEFAULT NULL,
  `pilihan_b` varchar(200) DEFAULT NULL,
  `pilihan_c` varchar(200) DEFAULT NULL,
  `pilihan_d` varchar(200) DEFAULT NULL,
  `kunci_jawaban` varchar(50) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1:writing; 2:reading; 3:listening',
  `id_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tb_soal_tb_listening_group` (`id_group`),
  CONSTRAINT `FK_tb_soal_tb_listening_group` FOREIGN KEY (`id_group`) REFERENCES `tb_listening_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_soal: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_soal` DISABLE KEYS */;
INSERT INTO `tb_soal` (`id`, `pertanyaan`, `pilihan_a`, `pilihan_b`, `pilihan_c`, `pilihan_d`, `kunci_jawaban`, `level`, `type`, `id_group`) VALUES
	(3, 'apakah yang dimaksud dengan IT?', 'Information technology', 'Information Technikal', 'Information Trilogy', 'Information Tricky', 'A', 1, 2, NULL),
	(8, 'how to make bakery?', 'give', 'gave', 'goven', 'get', 'B', 1, 3, 8),
	(9, 'how to make cycle?', 'clean', 'clear', 'close', 'clin', 'A', 1, 3, 8),
	(10, 'ggcgc', 'lL', 'k', 'h', 'h', 'A', 1, 3, 9);
/*!40000 ALTER TABLE `tb_soal` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_tenses
CREATE TABLE IF NOT EXISTS `tb_tenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tenses` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_tenses: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_tenses` DISABLE KEYS */;
INSERT INTO `tb_tenses` (`id`, `nama_tenses`, `deskripsi`) VALUES
	(5, 'Simple Perfect Tense', '<p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="color: rgb(255, 102, 0);"><span style="font-weight: 700;">Kalimat Verball</span></span></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="font-weight: 700;">Catatan:</span>&nbsp;Present Perfect tense dibentuk dengan auxiliary verb “<em>have / has</em>” dan past participle (verb-3). Has digunakan untuk subjek (<em>She, He, It</em>) dan have digunakan untuk subjek (<em>I, you, we, they</em>) . Sedangkan&nbsp;<em>past participle</em>&nbsp;yang digunakan dapat berupa irregular atau regular verb.</p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"="">Rumus Past Perfect untuk kalimat positif, negatif, dan kalimat tanya.</p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="color: rgb(0, 128, 0);"><span style="font-weight: 700;">1. Kalimat Positif</span></span></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="font-weight: 700;">S + have / has + Past participle</span></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"="">Contoh :&nbsp;<em>She has worked in the hospital for six years</em></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I have fixed my computer</em></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="color: rgb(0, 128, 0);"><span style="font-weight: 700;">2. Kalimat Negatif</span></span></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="font-weight: 700;">S + have / has + not + Past participle</span></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"="">Contoh :&nbsp;<em>She hasn’t worked in the hospital.</em></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I have not fixed my computer.</em></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="font-weight: 700;"><span style="color: rgb(0, 128, 0);">3. Kalimat Tanya</span></span></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><span style="font-weight: 700;">Have / has &nbsp;+ S + Past participle</span></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"="">Contoh :&nbsp;&nbsp;<em>Has she worked in the hospital for six years?</em></p><p style="font-family: " source="" sans="" pro",="" sans-serif;="" font-size:="" 18px;="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" justify;"=""><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Have I fixed my computer?</em></p><p><em><br></em></p>'),
	(6, 'Simple Present Tense', 'ini tes updated'),
	(7, 'test tenses', 'ini test tenses');
/*!40000 ALTER TABLE `tb_tenses` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_type
CREATE TABLE IF NOT EXISTS `tb_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `deskripsi` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_type: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_type` DISABLE KEYS */;
INSERT INTO `tb_type` (`id`, `type`, `deskripsi`) VALUES
	(1, 1, 'writing'),
	(2, 2, 'reading'),
	(3, 3, 'listening');
/*!40000 ALTER TABLE `tb_type` ENABLE KEYS */;

-- membuang struktur untuk table course.tb_vocab
CREATE TABLE IF NOT EXISTS `tb_vocab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bentuk_dasar` varchar(50) DEFAULT NULL,
  `past_simple` varchar(50) DEFAULT NULL,
  `past_participle` varchar(50) DEFAULT NULL,
  `arti` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.tb_vocab: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tb_vocab` DISABLE KEYS */;
INSERT INTO `tb_vocab` (`id`, `bentuk_dasar`, `past_simple`, `past_participle`, `arti`) VALUES
	(2, 'begin', 'began', 'begun', 'mulai');
/*!40000 ALTER TABLE `tb_vocab` ENABLE KEYS */;

-- membuang struktur untuk table course.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(60) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Membuang data untuk tabel course.users: ~4 rows (lebih kurang)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `username`, `password`, `last_login`, `created_at`, `updated_at`) VALUES
	(10, 1, 'admin', '$2y$11$xI1CVMHb8kLoU8NqI7W9SeuQ76UnYIUIgq9MxqLcSEwTOzqTfTTnS', NULL, '2018-09-29 13:31:07', '2018-09-29 13:31:07'),
	(11, 2, '135150218113024', '$2y$11$xI1CVMHb8kLoU8NqI7W9SeuQ76UnYIUIgq9MxqLcSEwTOzqTfTTnS', NULL, '2018-09-30 13:28:35', '2018-09-30 13:28:35'),
	(12, 2, '135150218113025', '$2y$11$nA10ixU0xz0H0vVw5/jWjewyHHBaTQsxLZIjXJMEJElKOwNr2nsCK', NULL, '2018-10-15 14:58:16', '2018-10-15 14:58:16'),
	(13, 2, '123', '$2y$11$rz6mElLpzTxDSrGwxZUH0uM2s4LHIR1JVkNU6yBfyO3VVqZvTFOO2', NULL, '2018-10-21 12:37:06', '2018-10-21 12:37:06');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
