<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/err_login', ['as'=>'err_login' , 'uses'=>'AuthController@err_login']);
Route::post('/auth', ['as'=>'auth' , 'uses'=>'AuthController@do_login']);
Route::get('/dashboard', ['as'=>'dashboard' , 'uses'=>'HomeController@index']);
Route::get('/auth/change_password', ['as'=>'auth/change_password' , 'uses'=>'AuthController@change_password']);
Route::post('/auth/save_password', ['as'=>'auth/save_password' , 'uses'=>'AuthController@save_password']);
Route::get('/logout', ['as'=>'logout' , 'uses'=>'AuthController@logout']);

// CRUD SISWA
Route::get('/siswa', ['as'=>'siswa' , 'uses'=>'SiswaController@index']);
Route::get('/siswa/add', ['as'=>'siswa/add' , 'uses'=>'SiswaController@add']);
Route::post('/siswa/proc_add', ['as'=>'siswa/proc_add' , 'uses'=>'SiswaController@proc_add']);
Route::get('/siswa/edit/{nis}', ['as'=>'siswa/edit/{nis}' , 'uses'=>'SiswaController@edit']);
Route::post('/siswa/proc_update', ['as'=>'siswa/proc_update' , 'uses'=>'SiswaController@proc_update']);
Route::get('/siswa/delete/{nis}', ['as'=>'siswa/delete/{nis}' , 'uses'=>'SiswaController@delete']);

// CRUD Tenses
// Route::get('/tenses/generate', ['as'=>'tenses/generate' , 'uses'=>'TensesController@generate_pass']);
Route::get('/tenses', ['as'=>'tenses', 'uses'=>'TensesController@index']);
Route::get('/tenses/detail/{id}', ['as'=>'tenses/detail/{id}' , 'uses'=>'TensesController@detail']);
Route::get('/tenses/add', ['as'=>'tenses/add' , 'uses'=>'TensesController@add']);
Route::post('/tenses/proc_add', ['as'=>'tenses/proc_add' , 'uses'=>'TensesController@proc_add']);
Route::get('/tenses/edit/{id}', ['as'=>'tenses/edit/{id}' , 'uses'=>'TensesController@edit']);
Route::post('/tenses/proc_update', ['as'=>'tenses/proc_update' , 'uses'=>'TensesController@proc_update']);
Route::get('/tenses/delete/{id}', ['as'=>'tenses/delete/{id}' , 'uses'=>'TensesController@delete']);

// CRUD Vocab
Route::get('/vocab', ['as'=>'vocab' , 'uses'=>'VocabController@index']);
Route::get('/vocab/add', ['as'=>'vocab/add' , 'uses'=>'VocabController@add']);
Route::post('/vocab/proc_add', ['as'=>'vocab/proc_add' , 'uses'=>'VocabController@proc_add']);
Route::get('/vocab/delete/{id}', ['as'=>'vocab/delete/{id}' , 'uses'=>'VocabController@delete']);

// CRUD Materi
Route::get('/writing', ['as'=>'writing' , 'uses'=>'MateriController@writing']);
Route::get('/reading', ['as'=>'reading' , 'uses'=>'MateriController@reading']);
Route::get('/listening', ['as'=>'listening' , 'uses'=>'MateriController@listening']);

Route::get('/materi', ['as'=>'materi' , 'uses'=>'MateriController@index']);
Route::get('/materi/test/{type}', ['as'=>'materi/test/{type}' , 'uses'=>'MateriController@test']);
Route::get('/materi/detail/{id}', ['as'=>'materi/detail/{id}' , 'uses'=>'MateriController@detail']);
Route::get('/materi/add/{type}', ['as'=>'materi/add/{type}' , 'uses'=>'MateriController@add_materi']);
Route::get('/materi/edit/{id}', ['as'=>'materi/edit/{id}' , 'uses'=>'MateriController@edit']);
Route::post('/materi/save_materi', ['as'=>'materi/save_materi' , 'uses'=>'MateriController@save_materi']);
Route::post('/materi/proc_update', ['as'=>'materi/proc_update' , 'uses'=>'MateriController@proc_update']);
Route::get('/materi/delete/{id}', ['as'=>'materi/delete/{id}' , 'uses'=>'MateriController@delete']);
Route::post('/materi/komentar', ['as'=>'materi/komentar' , 'uses'=>'MateriController@komentar']);
Route::post('/materi/hitung_skor', ['as'=>'materi/hitung_skor' , 'uses'=>'MateriController@hitung_skor']);
Route::get('/materi/detail_soal/{type}', ['as'=>'materi/detail_soal/{type}' , 'uses'=>'MateriController@detail_soal']);
Route::get('/materi/add_soal/', ['as'=>'materi/add_soal' , 'uses'=>'MateriController@add_soal']);
Route::post('/materi/proc_add_soal', ['as'=>'materi/proc_add_soal' , 'uses'=>'MateriController@proc_add_soal']);
Route::post('/materi/proc_update_kunci', ['as'=>'materi/proc_update_kunci' , 'uses'=>'MateriController@proc_update_kunci']);
Route::get('/materi/delete_soal/{id}/{type}', ['as'=>'materi/delete_soal/{id}/{type}' , 'uses'=>'MateriController@delete_soal']);

// Seleksi
Route::get('/seleksi', ['as'=>'seleksi' , 'uses'=>'SeleksiController@index']);
Route::get('/seleksi/kategori_soal/{level}', ['as'=>'seleksi/kategori_soal/{level}' , 'uses'=>'SeleksiController@kategori_soal']);;
Route::get('/seleksi/detail/{level}/{type}', ['as'=>'seleksi/detail/{level}/{type}' , 'uses'=>'SeleksiController@detail']);
// Route::get('/seleksi/delete/{id}', ['as'=>'seleksi/delete/{id}' , 'uses'=>'SeleksiController@delete']);
Route::get('/seleksi/add/{level}', ['as'=>'seleksi/add/{level}' , 'uses'=>'SeleksiController@add']);
Route::post('/seleksi/proc_add', ['as'=>'seleksi/proc_add' , 'uses'=>'SeleksiController@proc_add']);
Route::post('/seleksi/save_seleksi', ['as'=>'seleksi/save_seleksi' , 'uses'=>'SeleksiController@save_seleksi']);
Route::get('/seleksi/do_seleksi/{level}', ['as'=>'seleksi/do_seleksi/{level}' , 'uses'=>'SeleksiController@do_seleksi']);
Route::post('/seleksi/hitung_skor', ['as'=>'seleksi/hitung_skor' , 'uses'=>'SeleksiController@hitung_skor']);
Route::get('/seleksi/list_skor/{nis}', ['as'=>'seleksi/list_skor/{nis}' , 'uses'=>'SeleksiController@list_skor']);
Route::get('/seleksi/delete_soal/{id}/{lv}/{type}', ['as'=>'seleksi/delete_soal/{id}/{lv}/{type}' , 'uses'=>'SeleksiController@delete_soal']);

// Rekapitulasi
Route::get('/rekapitulasi', ['as'=>'rekapitulasi' , 'uses'=>'RekapitulasiController@index']);
Route::get('/rekapitulasi_materi', ['as'=>'rekapitulasi_materi' , 'uses'=>'RekapitulasiController@rekap_materi']);
