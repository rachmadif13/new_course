@extends('layouts.master')
@section('title','skor')
@section('css')
<style type="text/css">
    .table>tbody>tr>td{
        border:none;
    }

    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Skor Nilai</h3>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content" style="text-align: center;">
                        <img src="{{ asset('upload_img/skor.png') }}" style="width:150px; height:150px;">
                        <div style="margin:center;">
                            <table style="font-size: 20px;">
                            <tr style="text-align: left;">
                                <td>Skor Nilai</td>
                                <td>:</td>
                                <td>&nbsp;<?= $nilai; ?></td>
                            </tr>
                            <tr style="text-align: left;">
                                <td>Benar</td>
                                <td>:</td>
                                <td>&nbsp;<?= $benar; ?></td>
                            </tr>
                            <tr style="text-align: left;">
                                <td>Salah</td>
                                <td>:</td>
                                <td>&nbsp;<?= $salah; ?></td>
                            </tr>
                            <tr style="text-align: left;">
                                <td>Tidak dijawab</td>
                                <td>:</td>
                                <td>&nbsp;<?= $kosong; ?></td>
                            </tr>
                            <tr style="text-align: left;">
                                <td>Grade</td>
                                <td>:</td>
                                <td>&nbsp;
                                    <?php if($nilai < 35) : ?>
                                        E
                                    <?php elseif($nilai >= 35 && $nilai <= 44 ) : ?>
                                        D
                                    <?php elseif($nilai >= 45 && $nilai <= 54 ) : ?>
                                        C
                                    <?php elseif($nilai >= 55 && $nilai <= 64 ) : ?>
                                        C+
                                    <?php elseif($nilai >= 65 && $nilai <= 74 ) : ?>
                                        B
                                    <?php elseif($nilai >= 75 && $nilai <= 84 ) : ?>
                                        B+
                                    <?php elseif($nilai >= 85 && $nilai <= 100 ) : ?>
                                        A
                                    <?php endif; ?>   
                                </td>
                            </tr>
                            </table>
                        </div>
                    </div>
                    <?php if(Session::get("dif") == 'materi'): ?>
                        <a href="{{ url('materi') }}" class="btn btn-primary">Back To Materi</a>
                    <?php else : ?>
                        <a href="{{ url('seleksi') }}" class="btn btn-primary">Back To seleksi</a>
                    <?php endif; ?>
                </div>
        </div>
    </div>
@endsection

@section('script')
@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
@endsection
