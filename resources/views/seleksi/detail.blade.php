@extends('layouts.master')
@section('title','detail seleksi')
@section('css')
<style type="text/css">
    .table>tbody>tr>td{
        border:none;
    }
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Soal <?php 
                if($type == 1) { $jenis= "writing" ; ?>
                <?php }else if($type == 2) { $jenis= "reading" ; ?>
                <?php }else { $jenis= "listening" ; } ?>

                <?= $jenis; ?>
                 Level <?= $level; ?></h3>
          <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{ route('seleksi') }}">seleksi</a></li>
              <li><a href="{{ url('seleksi/detail/'.$level.'/'.$type) }}">level <?= $level; ?></a></li>
              <li class="active"><a><?=$jenis?></a></li>
          </ol>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('seleksi/save_seleksi')}}" method="POST">
                {{ csrf_field() }}
                <div class="x_panel">
                    <div class="x_title">
                        <h2 style="color: black; float:none; text-align: center;">Soal level <?=$level;?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <table class="table" style="border:none;">
                            <input type="hidden" name="type" value="<?=$type; ?>" >
                            <input type="hidden" name="level" value="<?= $level; ?>" >
                            <?php 
                            $no     = 1; 
                            $q      = 1; 
                            $x =1;
                            foreach($data as $key => $val):?>
                            <tr>
                                <td colspan="3">
                                    <?php if($type==3) : ?>
                                        <audio controls>
                                            <source src="{{ asset('upload_audio/'.$data[$key][0]->audio)}}" type="audio/mpeg">
                                        </audio> <br>
                                    <?php elseif($type==2) :?>
                                       <i><b>Question </b></i> <br><?= $data[$key][0]->bacaan;?> <br>
                                    <?php else :?>
                                        <?php echo "";?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                                <?php foreach($val as $key2 => $val2) :?>
                                    <input type="hidden" name="id[]" value="<?=$val2->id; ?>" >
                                    <tr>
                                        <td style="width: 2%;"><?= $no++."."; ?></td>
                                        <td><?= $val2->pertanyaan;?>  <a href="{{ route('seleksi/delete_soal/{id}/{lv}/{type}', ['id' => $val2->id, 'lv' => $level, 'type' => $type]) }}" class="btn btn-xs btn-danger" > <i class="fa fa-trash"></i> hapus</a> <br></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td <?php if($val2->kunci_jawaban == "A"){ echo "style='color:#0fc52e;font-weight:bold;'";} ?> >A. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="A" <?php if($val2->kunci_jawaban=='A'){ echo "checked"; }?> > <?= $val2->pilihan_a;?>
                                        </td>
                                        <td <?php if($val2->kunci_jawaban == "B"){ echo "style='color:#0fc52e;font-weight:bold;'";} ?> >B. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="B" <?php if($val2->kunci_jawaban=='B'){ echo "checked"; }?> > <?= $val2->pilihan_b;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td <?php if($val2->kunci_jawaban == "C"){ echo "style='color:#0fc52e;font-weight:bold;'";} ?> >C. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="C" <?php if($val2->kunci_jawaban=='C'){ echo "checked"; }?> > <?= $val2->pilihan_c;?>
                                        </td>
                                        <td <?php if($val2->kunci_jawaban == "D"){ echo "style='color:#0fc52e;font-weight:bold;'";} ?> >D. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="D" <?php if($val2->kunci_jawaban=='D'){ echo "checked"; }?> > <?= $val2->pilihan_c;?>
                                        </td>
                                    </tr>
                                <?php endforeach; $x++;?>
                            <?php endforeach; ?>
                        </table>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary pull-right" id="save"> <i class="fa fa-save"></i> Save Kunci Jawaban</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
<!-- <script src="{{ asset('gentelella/plugins/iCheck/icheck.min.js')}}"></script> -->
<script type="text/javascript">
    var soundFx = $( '#soundFx' ); // Get our sound FX.
    soundFx[0].play(); // Plays sound.
    var target = $( 'input' );
    target.on( 'change', function(){
    soundFx[0].play();
    });
</script>
@endsection
