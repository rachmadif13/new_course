@extends('layouts.master')
@section('title','Add Seleksi')
@section('css')
<style type="text/css">
label, h2, h3, table, a{
color: black;
}
</style>
@endsection
@section('content')
<div class="page-title">
    <div class="title_left">
        <h3>Add Seleksi</h3>
        <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
            <li><a href="{{ route('dashboard') }}">Home</a></li>
            <li><a href="{{ route('seleksi') }}">seleksi</a></li>
            <li><a href="{{ url('seleksi/kategori_soal/'.$level) }}">level <?= $level; ?></a></li>
            <li class="active"><a>add</a></li>
        </ol>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Add Seleksi Level <?= $level ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" method="POST" action="{{ route('seleksi/proc_add') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="level" class="form-control" value="<?=$level; ?>">
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Jenis soal<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="jenis_soal" class="form-control" onchange="listen()" id="jenis_soal">
                                <option disabled="" selected=""> Pilih jenis soal </option>
                                <option value="1"> Writing </option>
                                <option value="2"> Reading </option>
                                <option value="3"> Listening </option>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <button class="btn btn-primary add-more" type="button"><i class="glyphicon glyphicon-plus"></i> add soal</button>
                            <button type="submit" class="btn btn-success" id="save"><i class="fa fa-save"></i> Submit</button>
                        </div>
                    </div>
                    <div class="form-group" id="audi" style="display: none;">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Upload Audio <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" name="audio" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="bacaan" style="display: none;">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Teks Bacaan<span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="bacaan" class="form-control" rows ='5'></textarea>
                        </div>
                    </div>
                    <div class="after-add-more">
                    </div>
                        <div class="form-group" id="pertanyaan">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Pertanyaan<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="pertanyaan[]" class="form-control" rows ='5'></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">A</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="pilihan_a[]" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">B</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="pilihan_b[]" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">C</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="pilihan_c[]" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">D</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="pilihan_d[]" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Kunci Jawaban <span class="required"> * </span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="kunci[]" class="form-control">
                                    <option value="A"> A </option>
                                    <option value="B"> B </option>
                                    <option value="C"> C </option>
                                    <option value="D"> D </option>
                                </select>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Copy field -->
<div class="copy hide">
    <div class="form-group" id="pertanyaan">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Pertanyaan<span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="pertanyaan[]" class="form-control" rows ='5'></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">A</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="pilihan_a[]" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">B</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="pilihan_b[]" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">C</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="pilihan_c[]" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Pilihan <span class="required">D</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="pilihan_d[]" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Kunci Jawaban <span class="required"> * </span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <select name="kunci[]" class="form-control">
                <option value="A"> A </option>
                <option value="B"> B </option>
                <option value="C"> C </option>
                <option value="D"> D </option>
            </select>
        </div>
    </div>
</div>
<!-- End copy field -->
@endsection
@section('script')
<script type="text/javascript">
    function listen(){
        var opsi = $("#jenis_soal").val() ;
        if(opsi == 3){
            $("#audi").show();
            $("#bacaan").hide();
        }else if(opsi == 2){
            $("#bacaan").show();
            $("#audi").hide();
        }else{
            $("#audi").hide();
            $("#bacaan").hide();
        }
    }

$(document).ready(function() {
    $(".add-more").click(function(){
    var html = $(".copy").html();
        $(".after-add-more").after(html);
    });
    $("body").on("click",".remove",function(){
         $(this).parents(".control-group").remove();
    });
});
</script>
@endsection