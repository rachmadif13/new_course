@extends('layouts.master')

@section('title','Seleksi')

@section('css')
    <link href="{{ asset('gentelella/plugins/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style type="text/css">
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Seleksi Level <?=$level;?> </h3>
          <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{ route('seleksi') }}">seleksi</a></li>
              <li class="active"><a>level <?= $level; ?></a></li>
          </ol>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <?php if(Session::get('role_id')==1) : ?>
                        <div class="navbar-right" id="bnav_head">
                            <a href=" {{ route('seleksi/add/{level}', ['level'=>$level]) }}">
                                <button type="button" class="btn btn-sm btn-primary">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th width="2%">No.</th>
                                <th width="25%">Kategori Soal</th>
                                <th width="25%">Jumlah Soal</th>
                                <th width="25%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach($data as $row ) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row->deskripsi; ?></td>
                                <td><?= $row->jml_soal; ?></td>
                                <td style="text-align:center;">
                                    <a href="{{ route('seleksi/detail/{level}/{type}', ['level'=>$row->level, 'type'=>$row->type]) }}" class="btn btn-success btn-xs">
                                        <i class="fa fa-info"></i> Detail
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- datatables -->
    <script src="{{ asset('gentelella/plugins/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('gentelella/plugins/datatables/js/dataTables.bootstrap.js')}}?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>

@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
@endsection