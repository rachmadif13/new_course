@extends('layouts.master')
@section('title','Detail Materi')
@section('css')
<style type="text/css">
        label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Detail Materi <?php if($materi->type == 1) :
                $type = "writing";
                elseif($materi->type == 2) :
                $type = "reading";
                else :
                $type = "listening";
                endif;?> <?= $type ?></h3>
          <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{ route($type) }}"><?= $type; ?> </a></li>
              <li><a class="active"><?= $materi->judul_materi ?></a></li>
          </ol>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="color: black; float:none; text-align: center;">{{ $materi->judul_materi }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="color: #000000;">
                    <?= $materi->deskripsi; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <h3>Komentar </h3>
            <form action="{{ route('materi/komentar') }}" method="POST">            
                {{ csrf_field() }}
                <input type="hidden" name="id_materi" value="<?= $materi->id; ?>">
                <input type="hidden" name="id_user" value="<?= Session::get('id');?>">
                <textarea name="pesan" class="form-control" rows=3 style="width: 50%;"></textarea>
                <button type="submit" name="b_comment" class="btn btn-success btn-xs" style="margin-top:5px; "> submit </button>
            </form>
             <ul class="messages col-md-offset-1">
                <?php foreach($komentar as $row) : ?>
                <li>
                    <?php if(Session::get('role_id')==1) : ?>
                        <img src="{{ asset('gentelella/images/admin.png') }}" class="avatar">
                    <?php else : ?>
                        <img src="{{ asset('gentelella/images/user.png') }}" class="avatar">
                    <?php endif; ?>

                    <div class="message_wrapper">
                      <h5 class="heading"><?= $row->username; ?></h5>
                      <blockquote class="message" style="font-size: 12px;"><?= $row->pesan; ?></blockquote>
                      <br>
                      <p class="url">
                        <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                        <a href="#" style="font-size: 12px;"><i class="fa fa-calendar"></i><?= $row->created_at; ?></a>
                      </p>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
@endsection