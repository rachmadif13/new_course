@extends('layouts.master')

@section('title','Materi')

@section('css')
    <link href="{{ asset('gentelella/plugins/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style type="text/css">
        label, h2, h3, table{
            color: black;
        }
    </style>
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Materi <?= $type; ?></h3>
            <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}" style="color: black;">Home</a></li>
              <li><a style="color: black;"><?= $type; ?> </a></li>
            </ol>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Materi <?= $type; ?></h2>
                    <?php if(Session::get('role_id')==1) : ?>
                        <div class="navbar-right" id="bnav_head">
                            <a href=" {{ route('materi/add/{type}', ['type'=>$type]) }}">
                                <button type="button" class="btn btn-sm btn-primary">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th width="2%">No.</th>
                                <th width="15%">Judul Materi</th>
                                <th width="10%">Pembuat</th>
                                <th width="10%">Tanggal dibuat</th>
                                <th width="5%">Dilihat</th>
                                <th width="15%" style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach($materi as $row ) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row->judul_materi; ?></td>
                                <td><?= $row->created_by; ?></td>
                                <td><?= $row->created_at; ?></td>
                                <td><span class="label label-info"> <?= $row->dilihat; ?> kali </span></td>
                                <td style="text-align:center;">
                                    <?php if(Session::get('role_id')==1) : ?>
                                    <a href="{{ route('materi/detail/{id}', ['id' => $row->id]) }}" class="btn btn-success btn-xs">
                                        <i class="fa fa-info"></i> Detail
                                    </a>
                                    <a href="{{ route('materi/edit/{id}', ['id' => $row->id]) }}" class="btn btn-warning btn-xs">
                                        <i class="fa fa-pencil"></i> Update
                                    </a>
                                    <a href="{{ route('materi/delete/{id}', ['id' => $row->id]) }}" onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash"></i> Delete
                                    </a>
                                    <?php else: ?>
                                    <a href="{{ route('materi/detail/{id}', ['id' => $row->id]) }}" class="btn btn-success btn-xs">
                                        <i class="fa fa-info"></i> Detail
                                    </a>
                                <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- datatables -->
    <script src="{{ asset('gentelella/plugins/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('gentelella/plugins/datatables/js/dataTables.bootstrap.js')}}?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>

@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
@endsection