@extends('layouts.master')
@section('title','Do seleksi')
@section('css')
<style type="text/css">
    .table>tbody>tr>td{
        border:none;
    }
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Latihan Soal Materi  <?= $type; ?></h3>
            <?php if($type == 'writing'): $level = 1; ?>
            <?php elseif($type == 'reading') : $level = 2; ?>
            <?php elseif($type == 'listening') : $level = 3; ?>
            <?php endif;?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row" oncontextmenu="return false;">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <form action="{{ route('materi/hitung_skor')}}" method="POST" id="d_sf">
                {{ csrf_field() }}
                <input type="hidden" name="level" value="<?= $level; ?>" >

                <div class="x_panel">
                    <div class="x_title">
                    <div id="timer"></div>
                        <h2 style="color: black; float:none; text-align: center;">
                            Latihan Soal Materi  <?= $type; ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table class="table" style="border:none;">
                            <?php 
                            $no     = 1; 
                            $q      = 1; 
                            foreach($data as $key => $val):?>
                            <tr>
                                <td colspan="3">
                                    <?php if($data[$key][0]->type==3) : ?>
                                        <audio controls>
                                            <source src="{{ asset('upload_audio/'.$data[$key][0]->audio)}}" type="audio/mpeg">
                                        </audio> <br>
                                    <?php elseif($data[$key][0]->type==2) :?>
                                       <i><b>Question </b></i> <br><?= $data[$key][0]->bacaan;?> <br>
                                    <?php else :?>
                                        <?php echo "";?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                                <?php foreach($val as $key2 => $val2) :?>
                                    <input type="hidden" name="id[]" value="<?=$val2->id; ?>" >
                                    <tr>
                                        <td style="width: 2%;"><?= $no++."."; ?></td>
                                        <td><?= $val2->pertanyaan;?> </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>A. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="A"> <?= $val2->pilihan_a;?>
                                        </td>
                                        <td>B. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="B"> <?= $val2->pilihan_b;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>C. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="C"> <?= $val2->pilihan_c;?>
                                        </td>
                                        <td>D. <input type="radio" name="pilihan[<?= $val2->id;?>]" value="D"> <?= $val2->pilihan_d;?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </table>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary pull-right" id="save" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda?')"> <i class="fa fa-save"></i> Save Latihan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
  <!-- Script Timer -->
     <script type="text/javascript">
        $(document).ready(function() {
            var detik = 0;
            <?php if($type == 'writing'): ?>
            var menit = 30;
            <?php elseif($type == 'reading') : ?>
            var menit = 30;
            <?php elseif($type == 'listening') : ?>
            var menit = 30;
            <?php endif;?>

            function hitung() {
                setTimeout(hitung,1000);
                 $('#timer').html(
                      '<font style="color:black;"> Sisa waktu anda <br />' + menit + ' menit : ' + detik + ' detik </font>'
                );
                detik --;
                if(detik < 0) {
                    detik = 59;
                    menit --;
                    if(menit < 0) {
                        menit = 59;
                    } 
                } 
              if(detik == 0 && menit == 0){
                  $("#d_sf").submit();
              }
            }           
            hitung();
      }); 
        

    </script>

    <script type="text/javascript">
    document.onkeydown = function() {    
        switch (event.keyCode) { 
            case 116 : //F5 button
                event.returnValue = false;
                event.keyCode = 0;
                return false; 
            case 82 : //R button
                if (event.ctrlKey) { 
                    event.returnValue = false; 
                    event.keyCode = 0;  
                    return false; 
                } 
        }
    }
    window.history.forward();
</script>
@endsection
