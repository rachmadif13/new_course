@extends('layouts.master')

@section('title','Materi')

@section('css')
    <link href="{{ asset('gentelella/plugins/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <style type="text/css">
        label, h2, h3, table{
            color: black;
        }
    </style>
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Materi</h3>
            <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}" style="color: black;">Home</a></li>
              <li><a style="color: black;">materi</a></li>
            </ol>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Materi</h2>
                    <?php if(Session::get('role_id')==1) : ?>
                    <a href="{{ route('materi/add_soal') }} " class="btn btn-primary btn-xs" style="float: right;">
                        <i class="fa fa-plus"></i> Add soal </a>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th width="2%">No.</th>
                                <th width="15%">Judul Materi</th>
                                <th width="15%" style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            $lv = 1;
                            foreach($materi as $row ) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row->deskripsi; ?></td>
                                <td style="text-align:center;">
                                    <?php if(Session::get('role_id')==1) : ?>
                                    <a href="{{ url('') }}/<?=$row->deskripsi?>" class="btn btn-success btn-xs">
                                        <i class="fa fa-info"></i> Detail materi
                                    </a>
                                    <a href="{{ url('materi/detail_soal') }}/<?=$row->deskripsi?>" class="btn btn-primary btn-xs">
                                        <i class="fa fa-info"></i> Detail soal
                                    </a>
                                    <?php else : ?>                                            
                                        <?php if($lv==1 && $level_now->level == 1) { ?>
                                             <a href="{{ url('') }}/<?=$row->deskripsi?>" class="btn btn-success btn-xs">
                                                <i class="fa fa-info"></i> Detail
                                            </a>
                                             <a href="{{ url('materi/test/') }}/<?=$row->deskripsi?>" class="btn btn-primary btn-xs">
                                                <i class="fa fa-pencil"></i> kerjakan soal
                                            </a>
                                        <?php }if($lv==2 && $level_now->level == 2) { ?>
                                             <a href="{{ url('') }}/<?=$row->deskripsi?>" class="btn btn-success btn-xs">
                                                <i class="fa fa-info"></i> Detail
                                            </a>
                                             <a href="{{ url('materi/test/') }}/<?=$row->deskripsi?>" class="btn btn-primary btn-xs">
                                                <i class="fa fa-pencil"></i> kerjakan soal
                                            </a>
                                        <?php }if($lv==3 && $level_now->level == 3) { ?>
                                             <a href="{{ url('') }}/<?=$row->deskripsi?>" class="btn btn-success btn-xs">
                                                <i class="fa fa-info"></i> Detail
                                            </a>
                                             <a href="{{ url('materi/test/') }}/<?=$row->deskripsi?>" class="btn btn-primary btn-xs">
                                                <i class="fa fa-pencil"></i> kerjakan soal
                                            </a>
                                        <?php } ?>
                                        <?php if($level_now->level < $lv ) { ?>
                                                <a onclick="return confirm('Selesaikan level sebelumnya :)')" class="btn btn-success btn-xs" disabled>
                                                <i class="fa fa-info"></i> Detail
                                            </a>
                                                <a onclick="return confirm('Selesaikan level sebelumnya :)')" class="btn btn-primary btn-xs" disabled> <i class="fa fa-pencil"></i> kerjakan soal
                                                </a>
                                        <?php }elseif($level_now->level > $lv ) { ?>
                                             <a href="{{ url('') }}/<?=$row->deskripsi?>" class="btn btn-success btn-xs">
                                                <i class="fa fa-info"></i> Detail
                                            </a>
                                                <a href="{{ url('materi/test/') }}/<?=$row->deskripsi?>" class="btn btn-primary btn-xs">
                                                    <i class="fa fa-pencil"></i> kerjakan soal
                                                </a>
                                        <?php } ?>

                                    <?php endif; ?>

                                </td>
                            </tr>
                        <?php $lv++; endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- datatables -->
    <script src="{{ asset('gentelella/plugins/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('gentelella/plugins/datatables/js/dataTables.bootstrap.js')}}?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>

@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
@endsection