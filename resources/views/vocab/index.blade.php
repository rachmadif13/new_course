@extends('layouts.master')

@section('title','Vocabulary')

@section('css')
    <link href="{{ asset('gentelella/plugins/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style type="text/css">
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>List Vocab</h3><br>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List vocab</h2>
                    <?php if(Session::get('role_id')==1) : ?>
                        <div class="navbar-right">
                            <a href="{{ route('vocab/add') }}">
                                <button type="button" class="btn btn-sm btn-primary">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th width="25%">Bentuk Dasar</th>
                                <th width="25%">Past Simple</th>
                                <th width="25%">Past Participle</th>
                                <th width="25%">Arti</th>
                                <?php if(Session::get('role_id') == 1) : ?>
                                <th width="25%">Action</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($vocab as $row ) : ?>
                            <tr>
                                <td><?= $row->bentuk_dasar; ?></td>
                                <td><?= $row->past_simple; ?></td>
                                <td><?= $row->past_participle; ?></td>
                                <td><?= $row->arti; ?></td>
                                <?php if(Session::get('role_id') == 1) : ?>
                                <td> 
                                    <a href="{{ route('vocab/delete/{id}', ['id' => $row->id]) }}" class="btn btn-danger btn-xs"  onclick="return confirm('Apakah anda yakin?')"> <i class="fa fa-trash"></i> Delete</a>
                                </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- datatables -->
    <script src="{{ asset('gentelella/plugins/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('gentelella/plugins/datatables/js/dataTables.bootstrap.js')}}?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>

@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
@endsection