@extends('layouts.master')
@section('title','Add Vocabulary')
@section('css')
<style type="text/css">
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Add vocabulary</h3><br>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add vocabulary</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left" method="POST" action="{{ route('vocab/proc_add') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Bentuk dasar<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="text" name="bentuk_dasar" class="form-control" placeholder="Bentuk dasar">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Past Simple<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="text" name="past_simple" class="form-control" placeholder="Past simple">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Past Participle<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="text" name="past_participle" class="form-control" placeholder="Past Participle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Arti<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="text" name="arti" class="form-control" placeholder="Artinya">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                                <a href="#">
                                    <button type="button" class="btn btn-primary">Back</button>
                                </a>
                                <button type="submit" class="btn btn-success" id="save">Save</button>
                            </div>
                        </div>
                    </form>      
                </div>
            </div>
        </div>
    </div>
@endsection