@extends('layouts.master')

@section('title','List Siswa')

@section('css')
    <link href="{{ asset('gentelella/plugins/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style type="text/css">
@media print{
    #bnav_head, #act, #act_a, div.left_col .scroll-view, div.nav_menu, div.title_left, div.title_right, #b_print, footer, div.page-title, #print, #myTable_length, #myTable_filter,#myTable_info,#myTable_paginate, #aksi_l,#aksi_i {
        display: none;
    }
}
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>List Siswa</h3>
        <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
          <li><a href="{{ route('dashboard') }}">Home</a></li>
          <li class="active"><a> siswa </a></li>
        </ol>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                        <div class="navbar-right" id="bnav_head">
                            <a href=" {{ route('siswa/add') }}">
                                <button type="button" class="btn btn-sm btn-primary">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                            </a>
                                <button type="button" class="btn btn-sm btn-success" onclick="window.print()" id="print">
                                    <i class="fa fa-print"></i> Print
                                </button>
                        </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline" id="myTable">
                        <thead>
                            <tr>
                                <th width="2%">No.</th>
                                <th width="10%">NIS</th>
                                <th width="20%">Nama Lengkap</th>
                                <th width="2%">Jenis Kelamin</th>
                                <th width="10%">Tanggal Lahir</th>
                                <th width="20%">Alamat</th>
                                <th width="5%">Status</th>
                                <th width="10%">Foto</th>
                                <th width="3%" id="act">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach($siswa as $row ) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row->nis; ?></td>
                                <td><?= $row->nama; ?></td>
                                <td><?= $row->jenis_kelamin; ?></td>
                                <td><?= $row->tgl_lahir; ?></td>
                                <td><?= $row->alamat; ?></td>
                                <td><span class="label label-success"><?php if($row->status==1){ echo "Aktif"; }else { echo "Tidak Aktif";} ?> </span></td>
                                <td><img src="{{ asset('upload_img/'.$row->foto) }}" class="img img-responsive" style="height: 100px; width: 75px;"></td>
                                    <td id="act_a">
                                        <ul style="list-style: none;padding-left: 0px;padding-right: 0px; text-align: center;">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    <i class="fa fa-bars" style="font-size: large;"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" style="right: 0; left: auto;">
                                                    <li>
                                                        <a href="{{ route('siswa/edit/{nis}', ['nis' => $row->nis]) }}" style="color: black;">
                                                            <i class="fa fa-pencil"></i> Edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="{{ route('siswa/delete/{nis}', ['nis' => $row->nis]) }}" class="btn-delete" onclick="return confirm('Apakah anda yakin?')" style="color: black;">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- datatables -->
    <script src="{{ asset('gentelella/plugins/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('gentelella/plugins/datatables/js/dataTables.bootstrap.js')}}?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>

@if(Session::has('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;

<script type="text/javascript">
// $('#myTable').delegate('a.btn-delete', 'click', function(e){
//     e.preventDefault();
//     var id = $(this).data('id');
//     swal({
//         title: "Confirm Delete Data",
//         text: "Are you sure delete this data?",
//         type: "warning",
//         showCancelButton: true,
//         confirmButtonClass: 'btn-danger',
//         confirmButtonText: 'Delete',
//         cancelButtonText: "Cancel",
//         closeOnConfirm: false,
//         closeOnCancel: false
//     },
//     function(isConfirm){
//         if (isConfirm){
//             $.ajax({
//                 type: "POST",
//                 url: "{{ url('siswa/delete/') }}",
//                 data: {id: id},
//                 headers:{ 'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
//                 beforeSend: function() {},
//                 success: function(r) {
//                     if(r.error == false) {
//                         swal({
//                             title: 'Deleted', 
//                             text: r.message, 
//                             type: r.type,
//                         });
//                         setTimeout(function() {
//                             // window.location.href = "siswa";
//                         }, 2000);
//                     }
//                 },
//                 error: function(e) {}
//             });
//         } else {
//             swal("Failure", "Delete Cancel", "error");
//         }
//     });
// });
</script>
@endsection