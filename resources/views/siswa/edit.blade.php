@extends('layouts.master')
@section('title','Edit Siswa')
@section('css')
<style type="text/css">
        label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Edit Siswa</h3>
            <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li><a href="{{ route('siswa') }}"> siswa </a></li>
              <li class="active"><a> edit </a></li>
            </ol>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Silahkan update data siswa </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left" id="myForm" method="POST" action="{{ route('siswa/proc_update') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">NIS<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="text" name="nis" class="form-control" value="{{ $siswa->nis }}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Nama Lengkap<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="text" name="nama" class="form-control" value="{{ $siswa->nama }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Jenis Kelamin<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <select name="jenis_kelamin" class="form-control">
                                    <option value="L" <?php if($siswa->jenis_kelamin == "L"){ echo "selected";}?> >Laki-Laki</option>
                                    <option value="P" <?php if($siswa->jenis_kelamin == "P"){ echo "selected";}?> >Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Tanggal Lahir<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="date" name="tgl_lahir" class="form-control" value="{{ $siswa->tgl_lahir }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Alamat<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <textarea class="form-control" rows="3" name="alamat">{{ $siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Status<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <select name="status" class="form-control">
                                    <option value="1" <?php if($siswa->status == 1 ){ echo "selected"; }?> >Aktif</option>
                                    <option value="0" <?php if($siswa->status == 0 ){ echo "selected"; }?> >Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Foto<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                               <img src="{{ asset('upload_img/'.$siswa->foto) }}" class="img img-responsive" style="height: 150px; width: 100px;">
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                                <button type="submit" class="btn btn-success" id="save"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </form>      
                </div>
            </div>
        </div>
    </div>
@endsection