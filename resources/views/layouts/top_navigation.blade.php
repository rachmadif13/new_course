<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <?php if(Session::get('role_id')==1) : ?>
            <img src="{{ asset('gentelella/images/admin.png') }}" alt="">
            <?php else : ?>
            <img src="{{ asset('gentelella/images/user.png') }}" alt="">
            <?php endif; ?>
            <?php if(Session::get('role_id')==1) : ?>
                            {{Session::get('username')}}
                        <?php else: ?>
                            {{Session::get('nama_siswa')}}
                        <?php endif; ?> <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="{{route('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>