<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title">
<!--                 <i class="fa fa-paw"></i>
                 <span> Course</span> -->
                    <img src="{{ asset('upload_img/bec.png') }}" style="height: 80px; width: 150px;padding:10px;" class="img img-responsive"> 
            </a>
        </div>
        <div class="clearfix"></div>
        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
            <?php if(Session::get('role_id')==1) : ?>
                <img src="{{ asset('gentelella/images/admin.png') }}" alt="..." class="img-circle profile_img">
            <?php else : ?>
                <img src="{{ asset('gentelella/images/user.png') }}" alt="..." class="img-circle profile_img">
            <?php endif; ?>
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>
                        <?php if(Session::get('role_id')==1) : ?>
                            {{Session::get('username')}}
                        <?php else: ?>
                            {{Session::get('nama_siswa')}}
                        <?php endif; ?>
                </h2>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /menu profile quick info -->
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <?php if(Session::get('role_id')==1) : ?>
                        <li><a href="{{ route ('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                        <li><a href="{{ route ('siswa')}}"><i class="fa fa-users"></i> Manage Data Siswa </a></li>
                        <li><a href="{{ route ('materi')}}"><i class="fa fa-list-alt"></i>Manage Materi</a></li>
                        <li><a href="{{ route ('seleksi')}}"><i class="fa fa-th-large"></i> Manage Seleksi </a></li>
                        <li><a href="{{ route ('tenses') }}""><i class="fa fa-th-list"></i> Manage Tenses </a></li>
                        <li><a href="{{ route ('vocab')}}"><i class="fa fa-tags"></i> Vocabulary </a></li>
                        <li><a href="{{ route ('rekapitulasi_materi')}}"><i class="fa fa-tag"></i> Rekapitulasi materi</a></li>
                        <li><a href="{{ route ('rekapitulasi')}}"><i class="fa fa-book"></i> Rekapitulasi seleksi</a></li>
                    <?php else : ?>
                        <li><a href="{{ route ('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                        <li><a href="{{ route ('auth/change_password') }}"><i class="fa fa-key"></i> Ganti password </a></li>
                        <li><a href="{{ route ('tenses') }}"><i class="fa fa-th-list"></i> Tenses </a></li>
                        <li><a href="{{ route ('vocab')}}"><i class="fa fa-tags"></i> Vocabulary </a></li>
                        <li><a href="{{ route ('materi')}}"><i class="fa fa-list-alt"></i>Materi</a></li>
                        <li><a href="{{ route ('seleksi') }}"><i class="fa fa-th-large"></i>Seleksi</a></li>
                        <li><a href="{{ route('seleksi/list_skor/{nis}', ['nis' => Session::get('username')]) }}"> <i class="fa fa-list"></i> Skor seleksi</a>
                        </li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>