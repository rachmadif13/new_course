@extends('layouts.master')
@section('title','Change Password')
@section('css')
<link rel="stylesheet" href="{{ asset('gentelella/plugins/trumbowyg/dist/ui/trumbowyg.min.css') }}">
<style type="text/css">
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Ganti password</h3>
            <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li class="active"><a> ganti password </a></li>
            </ol>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Silahkan ganti password anda ! </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ route('auth/save_password') }}">
                        {{ csrf_field() }}  
                        <input type="hidden" name="id_user" value="<?= Session::get('username')?>">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Password Baru<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="password" name="first_password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Konfirmasi Password<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                <input type="password" name="second_password" class="form-control">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                                <button type="submit" class="btn btn-success" id="save"> <i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </form>      
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
  @if(Session::has('message')) : ?>
    <script type="text/javascript">
            swal({
              title: '<?=Session::get("type");?>',
              text: '<?=Session::get("message");?>',
              type: '<?= Session::get("type");?>',
              timer: 2000,
            });
    </script>  
  @endif;
@endsection