@extends('layouts.master')
@section('title','Add Tenses')

@section('css')
<link rel="stylesheet" href="{{ asset('gentelella/plugins/trumbowyg/dist/ui/trumbowyg.min.css') }}">
<style type="text/css">
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Add Tenses</h3><br>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Tenses</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left" method="POST" action="{{ route('tenses/proc_add') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Nama Tenses<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="nama" class="form-control" placeholder="Nama Tenses">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Deskripsi<span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea name="deskripsi" id="editor"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-2">
                                <a href="#">
                                    <button type="button" class="btn btn-primary">Back</button>
                                </a>
                                <button type="submit" class="btn btn-success" id="save">Save</button>
                            </div>
                        </div>
                    </form>      
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="{{ asset('gentelella/plugins/trumbowyg/dist/trumbowyg.min.js')}}"></script>
<script type="text/javascript">
        $('#editor').trumbowyg();
</script>
@endsection