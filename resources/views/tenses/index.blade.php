@extends('layouts.master')

@section('title','Manage Tenses')

@section('css')
    <link href="{{ asset('gentelella/plugins/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style type="text/css">
    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>List Tenses</h3><br>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List tenses</h2>
                    <?php if(Session::get('role_id')==1) : ?>
                        <div class="navbar-right" id="bnav_head">
                            <a href=" {{ route('tenses/add') }}">
                                <button type="button" class="btn btn-sm btn-primary">
                                    <i class="fa fa-plus"></i> add
                                </button>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th width="2%">No.</th>
                                <th width="25%">Nama Tenses</th>
                                <th width="15%" style="text-align:center;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach($tenses as $row ) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row->nama_tenses; ?></td>
                                <td style="text-align:center;">
                                    <a href="{{ route('tenses/detail/{id}', ['id' => $row->id]) }}" class="btn btn-success btn-xs">
                                        <i class="fa fa-info"></i> Detail
                                    </a>
                                    <?php if(Session::get('role_id')==1) : ?>
                                    <a href="{{ route('tenses/edit/{id}', ['id' => $row->id]) }}" class="btn btn-warning btn-xs">
                                        <i class="fa fa-pencil"></i> Update
                                    </a>
                                    <a href="{{ route('tenses/delete/{id}', ['id' => $row->id]) }}" onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash"></i> Delete
                                    </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- datatables -->
    <script src="{{ asset('gentelella/plugins/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('gentelella/plugins/datatables/js/dataTables.bootstrap.js')}}?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>

@if(Session::get('message')) : ?>
<script type="text/javascript">
        swal({
          title: '<?=Session::get("type");?>',
          text: '<?=Session::get("message");?>',
          type: '<?= Session::get("type");?>',
          timer: 2000,
        });
</script>  
@endif;
@endsection