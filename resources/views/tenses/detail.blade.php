@extends('layouts.master')
@section('title','Detail Tenses')
@section('css')
<style type="text/css">

    label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Detail Tenses</h3><br>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="color: black; float:none; text-align: center;">{{ $tenses->nama_tenses }}</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="color: #000000;">
                    <?= $tenses->deskripsi; ?>
                </div>
            </div>
        </div>
    </div>
@endsection