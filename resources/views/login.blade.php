<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- global stylesheets -->
    <link rel="stylesheet" type="text/css" href="{{ asset('gentelella/css/app.min.css') }}">
    <style type="text/css">
        /*preloader*/
        #preloader {position:fixed;top:0;left:0;right:0;bottom:0;background-color:#fff;z-index:9999999;}

        #status {width:100px;height:100px;position:absolute;left:47%;top:46%;background-repeat:no-repeat;background-position:center;}
        @media(max-width:320px){
            #status{left: 39%;top: 45%;}
        }
        @media screen and (min-width: 321px) and (max-width: 375px) {
            #status {left: 42%;}
        }
        @media screen and (min-width: 376px) and (max-width: 414px) {
            #status {left: 43%;}
        }
        @media screen and (min-width: 767px) and (max-width: 768px) {
            #status {left: 47%;}
        }
    </style>
    @yield('css')
  </head>
<body class="login">
        <!-- loader -->
        <div id="preloader">
        <div id="status">
            <div class="loader">
                <div class="square-spin">
                    <div style="background-color: #2A3F54;"></div>
                </div>
            </div>
        </div>
    </div>
      <div class="login_wrapper">
        <div class="animate form login_form" style="background: rgb(214, 233, 243) none repeat scroll 0% 0%;padding: 20px;">
          <section class="login_content">
            <form method="POST" action="{{ route('auth') }}">
              {{ csrf_field() }}
              <img src="{{ asset('gentelella/images/title_logo.png')}}">
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" name="username" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" name="password" />
              </div>
                <input type="submit" name="login" class="btn btn-primary col-md-12" value="Login" style="margin-left: 0px;">

              <div class="separator">

                <div class="clearfix"></div>
                <div><br>
                  <h1>English Course</h1>
                  <p>©2018 All Rights Reserved. English course</p>
                </div>
              </div>
            </form>
          </section>
        </div>

    <!-- global script -->
    <link rel="stylesheet" type="text/css" href="{{ asset('gentelella/css/app.min.css') }}">
    <script src="{{ asset('gentelella/js/app.min.js') }}"></script>
    
    <!-- loader -->
    <script type="text/javascript">
        $(window).load(function() { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(250).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(250).css({'overflow':'visible'});
        });
    </script>

  @if(Session::has('message')) : ?>
    <script type="text/javascript">
            swal({
              title: '<?=Session::get("type");?>',
              text: '<?=Session::get("message");?>',
              type: '<?= Session::get("type");?>',
              timer: 2000,
            });
    </script>  
  @endif;
  </body>
</html>

<head>
 