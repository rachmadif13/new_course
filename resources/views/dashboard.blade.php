@extends('layouts.master')

@section('title','Dashboard')

@section('css')
<style type="text/css">
        label, h2, h3, table, a{
        color: black;
    }
</style>
@endsection
@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>Dashboard</h3>
            <br>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Selamat datang <u> <?php if(Session::get('role_id')==1) : ?>
                            {{Session::get('username')}}
                        <?php else: ?>
                            {{Session::get('nama_siswa')}}
                        <?php endif; ?></u> di halaman dashboard.
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: black;">
                <div class="tile-stats">
                  <div class="icon" style="color: black;"><i class="fa fa-users"></i></div>
                  <div class="count"><?= $siswa; ?></div>
                  <h3 style="color: black;">Data Siswa</h3>
                  <p>Total data siswa course app</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: black;">
                <div class="tile-stats">
                  <div class="icon" style="color: black;"><i class="fa fa-book"></i></div>
                  <div class="count"><?= $materi; ?></div>
                  <h3 style="color: black;">Data Materi</h3>
                  <p>Total Materi course app</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: black;">
                <div class="tile-stats">
                  <div class="icon" style="color: black;"><i class="fa fa-pencil"></i></div>
                  <div class="count"><?= $soal; ?></div>
                  <h3 style="color: black;">Data Soal</h3>
                  <p>Total data soal course app</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: black;">
                <div class="tile-stats">
                  <div class="icon" style="color: black;"><i class="fa fa-check-square-o"></i></div>
                  <div class="count"><?= $tenses; ?></div>
                  <h3 style="color: black;">Data Tenses</h3>
                  <p>Total Tenses course app</p>
                </div>
              </div>
            </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
  @if(Session::has('message')) : ?>
    <script type="text/javascript">
            swal({
              title: '<?=Session::get("type");?>',
              text: '<?=Session::get("message");?>',
              type: '<?= Session::get("type");?>',
              timer: 2000,
            });
    </script>  
  @endif;
@endsection