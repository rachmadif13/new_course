@extends('layouts.master')

@section('title','Rekapitulasi')

@section('css')
    <link href="{{ asset('gentelella/plugins/datatables/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style type="text/css">
label, h2, h3, table, a{
color: black;
}
</style>
@endsection

@section('content')

    <div class="page-title">
        <div class="title_left">
            <h3>Rekapitulasi Materi</h3>
          <ol class="breadcrumb" style="background: transparent;padding-left: 0px;">
              <li><a href="{{ route('dashboard') }}">Home</a></li>
              <li class="active"><a>rekapitulasi materi </a></li>
          </ol>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th width="2%">No.</th>
                                <th width="25%">ID Siswa</th>
                                <th width="25%">Materi</th>
                                <th width="25%">Skor</th>
                                <th width="25%">Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach($list_skor as $row ) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row->id_siswa; ?></td>
                                <td><?php if($row->level == 1 ){
                                    echo "Writing";
                                }elseif($row->level == 2){
                                    echo "Writing";
                                }else{
                                    echo "Listening";
                                }  ?></td>
                                <td><?= $row->skor; ?></td>
                                <td>
                                    <?php if($row->skor < 35) : ?>
                             E
                        <?php elseif($row->skor >= 35 && $row->skor <= 44 ) : ?>
                             D
                        <?php elseif($row->skor >= 45 && $row->skor <= 54 ) : ?>
                             C
                        <?php elseif($row->skor >= 55 && $row->skor <= 64 ) : ?>
                            C+
                        <?php elseif($row->skor >= 65 && $row->skor <= 74 ) : ?>
                             B
                        <?php elseif($row->skor >= 75 && $row->skor <= 84 ) : ?>
                            B+
                        <?php elseif($row->skor >= 85 && $row->skor <= 100 ) : ?>
                             A
                        <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- datatables -->
    <script src="{{ asset('gentelella/plugins/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('gentelella/plugins/datatables/js/dataTables.bootstrap.js')}}?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>
@endsection